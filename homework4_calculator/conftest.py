import pytest
from calculator import Calculator
# ()加入autouse=True，会在每个方法前执行
# ()加入scope='module'，会在每个模块前执行
# ()加入scope='session'

@pytest.fixture(scope='module')
def calculate():
    print("开始计算")
    cal = Calculator()
    yield cal
    print('\n')
    print("结束计算")
    print('\n')