import pytest
from calculator import Calculator
#   ids可以重新命名名称
#   round可以定义小数点位数

class Testcal:

    #整数相加
    @pytest.mark.parametrize("a,b,expect",[[6,2,8],[1,2,3]],ids=['int1','int2'])
    def test_add_int(self, calculate, a, b, expect):
        cal = Calculator()
        assert expect == cal.add(a, b)

    #浮点数相加
    @pytest.mark.parametrize("a,b,expect",[[0.1,0.1,0.2],[0.1,0.2,0.3]],ids=['float1','float2'])
    def test_add_float(self, calculate, a, b, expect):
        cal = Calculator()
        assert expect == round(cal.add(a, b),2)

    #除数为0
    def test_div_zero(self,calculate):
        cal = Calculator()
        ##抛异常：第一种写法
        # with pytest.raises(ZeroDivisionError):
        #     cal.div(1,0)
        ##抛异常：第二种写法
        try:
            cal.div(1,0)
        except ZeroDivisionError:
            print("除数为0")

    #整数相除
    @pytest.mark.parametrize("a,b,expect",[[6,2,3]],ids=['div1'])
    def test_div(self, calculate, a, b, expect):
        cal = Calculator()
        assert expect == cal.div(a, b)

'''
打开Terminal
返回结果：pytest run.py --alluredir=./result
生成报告：allure generate ./result -o ./report --clean
查看报告：allure open -h 127.0.0.1 -p 8888 ./report

'''