#coding=utf-8
'''
python代码规范：
    1、模块：模块尽量使用小写命名，首字母保持小写，尽量不要用下划线（除非多个单词，且数量不多的情况）
    2、类名：类名使用驼峰(CamelCase)命名风格，首字母大写，私有类可用一个下划线开头
    3、函数：函数名一律小写，如有多个单词，用下划线隔开
    4、变量名：变量名尽量小写, 如有多个单词，用下划线隔开
    5、常量：常量采用全大写，如有多个单词，使用下划线隔开
快捷键：
    AIT + Enter键 快速添加路径
    CTRL + 鼠标左键 快速定位类方法
'''
class Hero:
    hp = 0
    power = 0
    name = ""
    def fight(self,enemy_hp,enemy_power):
        #我的最终血量 = 我的血量 - 敌人的攻击力
        final_hp = self.hp - enemy_power
        #敌人的最终血量 = 敌人的血量 - 我的的攻击力
        enemy_final_hp = enemy_hp - self.power
        if final_hp > enemy_final_hp:
            print("%s赢了"%(self.name))
            #python3.6以上才能用f
            #print(f"{self.name}赢了")

        elif final_hp < enemy_final_hp:
            print("敌人赢了")

        else:
            print("我们打平了")

    def speak_lines(self,name):
        if self.name == 'Timo':
            print("提莫队长正在待命...")
        elif self.name == 'Police':
            print("见识一下法律的子弹...")
        else:
            print("我一定会回来的！！！")


