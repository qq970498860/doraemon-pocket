#coding=utf-8
'''
python代码规范：
    1、模块：模块尽量使用小写命名，首字母保持小写，尽量不要用下划线（除非多个单词，且数量不多的情况）
    2、类名：类名使用驼峰(CamelCase)命名风格，首字母大写，私有类可用一个下划线开头
    3、函数：函数名一律小写，如有多个单词，用下划线隔开
    4、变量名：变量名尽量小写, 如有多个单词，用下划线隔开
    5、常量：常量采用全大写，如有多个单词，使用下划线隔开
快捷键：
    AIT + Enter键 快速添加路径
    CTRL + 鼠标左键 快速定位类方法
'''
from PycharmProjects.pythonProject.homework.homework2_hero_factory.hero import Hero

class Timo(Hero):
    hp = 1000
    power = 200
    name = "Timo"
