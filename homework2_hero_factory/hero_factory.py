#coding=utf-8
'''
python代码规范：
    1、模块：模块尽量使用小写命名，首字母保持小写，尽量不要用下划线（除非多个单词，且数量不多的情况）
    2、类名：类名使用驼峰(CamelCase)命名风格，首字母大写，私有类可用一个下划线开头
    3、函数：函数名一律小写，如有多个单词，用下划线隔开
    4、变量名：变量名尽量小写, 如有多个单词，用下划线隔开
    5、常量：常量采用全大写，如有多个单词，使用下划线隔开
快捷键：
    AIT + Enter键 快速添加路径
    CTRL + 鼠标左键 快速定位类方法
'''
import time
from PycharmProjects.pythonProject.homework.homework2_hero_factory.police import Police
from PycharmProjects.pythonProject.homework.homework2_hero_factory.timo import Timo


class HeroFactory:
    '''
    创建英雄工厂
    '''
    def creat_hero(self,name):
        if name == 'Timo':
            return Timo()
        if name == 'Police':
            return Police()
        else:
            raise Exception("此英雄不存在英雄工厂中")

#创建英雄（调用英雄）
hero_factory = HeroFactory()
police = hero_factory.creat_hero("Police")
timo = hero_factory.creat_hero("Timo")

#调用方法
print("杀戮正在高涨：")
time.sleep(1)
police.speak_lines(Police)
time.sleep(1)
timo.speak_lines(Timo)
time.sleep(1)
print("战斗即将开始：")
time.sleep(1)
print("3")
time.sleep(1)
print("2")
time.sleep(1)
print("1")
time.sleep(1)
police.fight(timo.hp,timo.power)


# 此英雄不存在
# ez = hero_factory.creat_hero("EZ")




