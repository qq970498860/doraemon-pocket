import time

from selenium.webdriver.common.by import By

from pythonProject.lesson.lesson5.lesson5_zhibo.page.base_page import BasePage
from pythonProject.lesson.lesson5.lesson5_zhibo.page.contact_page import ContactPage
from selenium import webdriver

class AddMemberPage(BasePage):
    def add_member(self,name):
        #输入姓名、账号、手机号
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[1]/div[1]/div[2]/div[1]/div/input").send_keys(name)
        time.sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[1]/div[2]/div[2]/input").send_keys("sanfeng")
        time.sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[2]/div[1]/div/div[1]/input").send_keys("13588889999")
        time.sleep(2)
        #点击保存 浏览器Console    搜索$(".qui_btn.ww_btn.js_btn_save")
        self.driver.find_element_by_css_selector(".qui_btn.ww_btn.js_btn_save").click()
        return ContactPage(self.driver)

    def add_member_fail(self,name):
        #输入姓名、账号、手机号
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[1]/div[1]/div[2]/div[1]/div/input").send_keys(name)
        time.sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[1]/div[2]/div[2]/input").send_keys("sanfeng")
        time.sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div/main/div/div/div[2]/div/div[4]/div/form/div[2]/div[2]/div[1]/div/div[1]/input").send_keys("13588889999")
        time.sleep(2)
        #点击保存 浏览器Console    搜索$(".qui_btn.ww_btn.js_btn_save")
        self.driver.find_element_by_css_selector(".qui_btn.ww_btn.js_btn_save").click()
        #找到错误提示信息
        ele_list = self.driver.find_elements_by_css_selector(".ww_inputWithTips_tips")
        error_list = [i.text for i in ele_list]
        print(error_list)

        #return ContactPage(self.driver)

    def goto_contact(self):
        pass