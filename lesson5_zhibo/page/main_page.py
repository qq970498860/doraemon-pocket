from selenium.webdriver.common.by import By

from pythonProject.lesson.lesson5.lesson5_zhibo.page.add_member_page import AddMemberPage
from selenium import webdriver

from pythonProject.lesson.lesson5.lesson5_zhibo.page.base_page import BasePage


class MainPage(BasePage):
    add_member_ele = (By.CSS_SELECTOR,".ww_indexImg_AddMember")
    def goto_add_member(self):

        # self.driver.find_element(By.CSS_SELECTOR,".ww_indexImg_AddMember").click()
        self.find(self.add_member_ele).click()

        return AddMemberPage(self.driver)

    def goto_contact(self):
        pass
