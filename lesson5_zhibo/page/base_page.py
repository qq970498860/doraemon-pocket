import time

from selenium import webdriver


class BasePage:
    '''
    封装页面通用方法，比如：driver的实例化
    '''
    def __init__(self,base_driver=None):
        # chrome_arg = webdriver.ChromeOptions()
        # chrome_arg.debuggerAddress = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome(options=chrome_arg)
        if base_driver is not None:
            self.driver = base_driver
        else:
            # 使用浏览器复用模式
            options = webdriver.ChromeOptions()
            # 加入调试模式
            options.debugger_address = "127.0.0.1:9222"
            # 实例化driver对象
            self.driver = webdriver.Chrome(options=options)
            #隐式等待
            self.driver.implicitly_wait(5)

    def find(self,locator):
        return self.driver.find_element(*locator)

