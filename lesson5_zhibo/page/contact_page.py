from pythonProject.lesson.lesson5.lesson5_zhibo.page.base_page import BasePage


class ContactPage(BasePage):
    def goto_add_member(self):
        pass

    def get_list(self):
        #浏览器Console    搜索$(".member_colRight_memberTable_td:nth-child(2)")
        ele_list = self.driver.find_elements_by_css_selector(".member_colRight_memberTable_td:nth-child(2)")
        name_list = []
        for i in ele_list:
            name_list.append(i.text)
        print(name_list)
        return name_list
